#include <stdio.h>

int main()
{
    int num,n1,n2,num1;
    int count = 1;
    int i, s, e;
    printf("Enter 1 for for loop code \n");
    printf("Enter 2 for while loop code \n");
    printf("Enter 3 for Range code \n");
     scanf("%d",&num);
    switch(num)
    {
        case 1:
            printf("Enter how many (Positive) Natural number you want to print : \t");
            scanf("%d",&n1);
            for(int i=1;i<=n1;i++)
            {
                printf("%d ",i);
            }
            break;
        case 2:
            printf("Enter how many (Positive) Natural number you want to print : \t ");
            scanf("%d",&num1);
             while(count <= num1)  
            {  
                printf("%d \t", count );  
                count++;  
            }  
        
        
            break;
        case 3:
            printf("Enter start value: ");
            scanf("%d", &s);
            printf("Enter end value: ");
            scanf("%d", &e);
            
            for(i=s; i<=e; i++)
            {
                printf("%d\t", i);
            }
            break;
    }

    return 0;
}
